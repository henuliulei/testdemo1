
import warnings
class DefaultConfig(object):
    model = 'LeNet5'
    lr = 1e-4
    Max_epoch = 10
    lr_decay = 0.95
    weight_decay = 1e-4
    use_gpu = True
    load_model_path = 'None'
    batch_size = 2
    num_workers = 0
    train_path = './rich/train/'
    val_path = './rich/val/'
    test_path = './rich/val/'

def parse(self,kwargs):
    for k,v in kwargs.items():
        if not hasattr(self,k):
            warnings.warn("Warning:opt has no attribute %s"%k)
        setattr(self,k,v)
DefaultConfig.parse = parse
opt = DefaultConfig()


