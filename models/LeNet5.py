import torch.nn as nn
from .BasicModule import BasicModule
class LeNet5(BasicModule):
    def __init__(self):
        super(LeNet5,self).__init__()
        self.conv1 = nn.Sequential(nn.Conv2d(3,6,5,1,0),nn.ReLU(),nn.MaxPool2d(2,2))
        self.conv2 = nn.Sequential(nn.Conv2d(6,16,5,1,0),nn.ReLU(),nn.MaxPool2d(2,2))
        self.fc1 = nn.Sequential(nn.Linear(250000,120),nn.BatchNorm1d(120),nn.ReLU())
        self.fc2 = nn.Sequential(nn.Linear(120,84),nn.BatchNorm1d(84),nn.ReLU())
        self.fc3 = nn.Sequential(nn.Linear(84,4),nn.BatchNorm1d(4),nn.ReLU())

    def forward(self,x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = x.view(x.size(0),-1)
        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)
        return x