import torch
from config import opt
class BasicModule(torch.nn.Module):
    def __init__(self):
        super(BasicModule,self).__init__()
        #self.model_name = str(type(self))

    def save(self,path=None):
        if path is None:
            path = 'checkpoints/'+opt.model+'_'+'model.pth'
        torch.save(self.state_dict(),path)
    def load(self,path):
        self.load_state_dict(torch.load(path),strict = False)


