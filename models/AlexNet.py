from .BasicModule import BasicModule
import torch.nn as nn
class AlexNet(BasicModule):
    def __init__(self,num_classes=4):
        super(AlexNet,self).__init__()
        self.features = nn.Sequential(
            nn.Conv2d(3,64,11,4,2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(3,2),
            nn.Conv2d(64,192,5,2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(3,2),
            nn.Conv2d(192,384,3,1),
            nn.ReLU(inplace=True),
            nn.Conv2d(384, 256, 3, 1),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, 3, 1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(3,2)
        )
        self.classifier = nn.Sequential(
            nn.Dropout(),
            nn.Linear(2304,4096),
            nn.ReLU(inplace=True),
            nn.Dropout(),
            nn.Linear(4096,4096),
            nn.ReLU(inplace=True),
            nn.Linear(4096,num_classes)
        )

    def forward(self,x):
        x = self.features(x)
        x = x.view(x.size()[0],-1)
        x = self.classifier(x)
        return x