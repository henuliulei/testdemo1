import warnings
warnings.filterwarnings("ignore")
from config import opt
import models
from torch.autograd import Variable
from PIL import Image
from torchvision import transforms
import torch
#测试单张图片
def test(img_path):
    opt.load_model_path = './checkpoints/LeNet5_model_F.pth'
    normalize = transforms.Normalize(
        mean=[0.485, 0.456, 0.406],
        std=[0.229, 0.224, 0.225]
    )
    transforms1 = transforms.Compose([
        transforms.Scale(512),
        transforms.CenterCrop(512),
        # transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        normalize
    ])
    model = getattr(models, opt.model)()
    model.eval()
    if opt.use_gpu:
        model.cuda()
    if opt.load_model_path:
        model.load(opt.load_model_path)
    label = int(img_path.split('/')[-1][0]) - 1
    data = Image.open(img_path)
    data = transforms1(data)
    data = data.cuda()
    data = Variable(data, requires_grad=False)
    data = data.unsqueeze(0)
    predict = model(data)
    _,index = torch.max(predict,1)
    print(index+1)

if __name__ == '__main__':
    img_path = "D:/Code/untitled/Rich/rich/train/2-2_26.jpg"
    test(img_path)