from visdom import Visdom
import torch
import numpy as np
viz = Visdom()
viz.line([0.],[0],win='loss',opts=dict(title='loss',legend=['loss']))
viz1 = Visdom()
viz1.line(np.column_stack(([0.],[0.])),[0],win='acc',opts=dict(title='acc',legend=['acc_train','acc_val']))

class visualize_loss:
    def __init__(self,loss,x):
        super(visualize_loss, self).__init__()
        viz.line([loss],[x],win='loss',update='append')
class visualize_val:
    def __init__(self,acc_train,acc_val,x):
        super(visualize_val, self).__init__()
        viz1.line(np.column_stack((acc_train,acc_val)),[x],win='acc',update='append')

