from torch.utils import data
import os
import torch
from torchvision import transforms
from PIL import Image
import numpy as np
class Rich(data.Dataset):
    def __init__(self,root,train=True,test = False,val = False,transform = None):

        imgs = [os.path.join(root,img) for img in os.listdir(root)]
        #np.random.seed(100)
        #imgs = np.random.permutation(imgs)
        #print(imgs)
        if train:
            self.imgs = imgs
        elif test:
            self.imgs = imgs[:int(0.7*len(imgs))]
        else:
            self.imgs = imgs[int(0.7*len(imgs)):]
        if transform is None:
            normalize = transforms.Normalize(
                mean=[0.485,0.456,0.406],
                std = [0.229,0.224,0.225]
            )
            if train:
                self.transforms = transforms.Compose([
                    transforms.Scale(512),
                    transforms.RandomSizedCrop(512),
                    transforms.RandomHorizontalFlip(),
                    transforms.ToTensor(),
                    normalize
                ])
            elif test:
                self.transforms = transforms.Compose([
                    transforms.Scale(512),
                    transforms.CenterCrop(512),
                    #transforms.RandomHorizontalFlip(),
                    transforms.ToTensor(),
                    normalize
                ])
            else:
                self.transforms = transforms.Compose([
                    transforms.Scale(512),
                    transforms.RandomSizedCrop(512),
                    transforms.RandomHorizontalFlip(),
                    transforms.ToTensor(),
                    normalize
                ])


    def __getitem__(self, index):
        img_path = self.imgs[index]#rich/train/1-1_1_00.jpg.jpg
        label = int(img_path.split('/')[-1][0])-1
        data = Image.open(img_path)
        data = self.transforms(data)
        return data,label
    def __len__(self):
        return len(self.imgs)








