#数据增强，翻转，旋转90，裁剪，加入噪声，在原始图像上增加图像，即训练集扩为四倍,
# 现在的train文件夹为扩充后的文件夹。只能运行一次，运行多次会在原基础上再次扩充。
import numpy
import cv2
import os
from PIL import Image
import numpy as np
import warnings
warnings.filterwarnings("ignore")
def Flip(path):
    im = Image.open(path)
    out = im.transpose(Image.FLIP_TOP_BOTTOM)
    out1 = im.transpose(Image.FLIP_LEFT_RIGHT)
    newPath = path.split('.')[0]+"_1"+"."+path.split('.')[1]
    newPath1 = path.split('.')[0]+"_2"+"."+path.split('.')[1]
    out.save(newPath)
    out1.save(newPath1)
def rotate(path):
    im = Image.open(path)
    out = im.rotate(90)
    out1 = im.rotate(270)
    newPath = path.split('.')[0] + "_3" + "." + path.split('.')[1]
    newPath1 = path.split('.')[0] + "_4" + "." + path.split('.')[1]
    out.save(newPath)
    out1.save(newPath1)
def crop(path):
    im = Image.open(path)
    #print(im.size[0])
    a,b = im.size[0],im.size[1]
    out = im.crop((0+0.25*a,0+0.25*b,a*0.75,b*0.75))
    newPath = path.split('.')[0] + "_5" + "." + path.split('.')[1]
    out.save(newPath)
def SaltAndPepper(src,percetage):
    src = cv2.imread(src)
    SP_NoiseImg=src
    SP_NoiseNum=int(percetage*src.shape[0]*src.shape[1])
    for i in range(SP_NoiseNum):
        randX=np.random.random_integers(0,src.shape[0]-1)
        randY=np.random.random_integers(0,src.shape[1]-1)
        if np.random.random_integers(0,1)==0:
            SP_NoiseImg[randX,randY]=0
        else:
            SP_NoiseImg[randX,randY]=255
    newPath = path.split('.')[0] + "_6" + "." + path.split('.')[1]
    cv2.imwrite(newPath,SP_NoiseImg)
def gasuss_noise(path, mean=0, var=0.001):
    '''
        添加高斯噪声
        mean : 均值
        var : 方差
    '''
    image = cv2.imread(path)
    image = np.array(image/255, dtype=float)
    noise = np.random.normal(mean, var ** 0.5, image.shape)
    out = image + noise
    if out.min() < 0:
        low_clip = -1.
    else:
        low_clip = 0.
    out = np.clip(out, low_clip, 1.0)
    out = np.uint8(out*255)
    #cv.imshow("gasuss", out)
    newPath = path.split('.')[0] + "_7" + "." + path.split('.')[1]
    cv2.imwrite(newPath, out)
    return out
def read(path):
    Flip(path)
    rotate(path)
    crop(path)
    SaltAndPepper(path, 0.05)
    gasuss_noise(path)
if __name__=='__main__':
    root = 'D:/Code/untitled/Rich/rich/train/'
    imgs = [os.path.join(root,img) for img in os.listdir(root)]
    for path in imgs:
        read(path)