import warnings
warnings.filterwarnings("ignore")
from config import opt
import os
import models
from rich.dataset import Rich
from torch.utils.data import DataLoader
import torch
from torchnet import meter
from tqdm import tqdm
from torch.autograd import Variable
from utils.visualize import visualize_loss
from utils.visualize import visualize_val

def train(**kwargs):
    opt.parse(kwargs)
    os.environ["CUDA_VISIBLE_DEVICES"] = '0'
    model = getattr(models,opt.model)()
    if opt.load_model_path:
        model.load(opt.load_model_path)
    if opt.use_gpu:
        model.cuda()
    train_data = Rich(opt.train_path,train=True,test=False,val=False)
    val_data = Rich(opt.val_path,train= False,test=False,val=True)
    train_loader = DataLoader(dataset=train_data,batch_size=opt.batch_size,
                              shuffle=False,num_workers=opt.num_workers,drop_last=True)
    val_loader = DataLoader(dataset=val_data,batch_size=opt.batch_size,
                            shuffle=False,num_workers=opt.num_workers,drop_last=True)
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(),lr = opt.lr,weight_decay=opt.weight_decay)
    loss_meter = meter.AverageValueMeter()
    confusion_matrix = meter.ConfusionMeter(4)
    previous_loss = 1e100
    num = 0
    for epoch in range(opt.Max_epoch):
        confusion_matrix.reset()
        loss_meter.reset()
        for ii, (data,label) in tqdm(enumerate(train_loader)):
            data = Variable(data)
            label = Variable(label.type(torch.LongTensor))
            if opt.use_gpu:
                data = data.cuda()
                label = label.cuda()
            optimizer.zero_grad()
            predict = model(data)
            loss = criterion(predict,label)
            loss.backward()
            optimizer.step()
            loss_meter.add(loss.item())
            num +=1
            if ii%10==0:

                visualize_loss(loss_meter.value()[0],num)
                print(epoch,"---------",loss_meter.value()[0])
                #loss_meter.reset()

        acc_val = val(model,val_loader)
        acc_train = val(model,train_loader)
        print("acc_val = " + str(acc_val))
        print("acc_train = " + str(acc_train))
        visualize_val(acc_train,acc_val,epoch+1)
        #visualize(acc_train,acc,epoch+1,train=False)
        model.save()
        if loss_meter.value()[0] < previous_loss:
            previous_loss = loss_meter.value()[0]
            opt.lr = opt.lr*opt.lr_decay

def val(model,val_loader):
    model.eval()
    confusion_matrix = meter.ConfusionMeter(4)
    for ii,(data,label) in enumerate(val_loader):
        if opt.use_gpu:
            data = data.cuda()
            label = label.cuda()
        data = Variable(data,requires_grad=False)
        label = Variable(label.type(torch.LongTensor),requires_grad=False)
        predict = model(data)
        confusion_matrix.add(predict.data.squeeze(),label)
    model.train()
    cm_value = confusion_matrix.value()
    accuracy = (cm_value[0][0] + cm_value[1][1] + cm_value[2][2] + cm_value[3][3])/cm_value.sum()
    return accuracy
def test(**kwargs):
    opt.parse(kwargs)
    model = getattr(models,opt.model)()
    model.eval()
    meter_loss = meter.AverageValueMeter()
    confusion_matrix = meter.ConfusionMeter(4)
    if opt.use_gpu:
        model.cuda()
    if opt.load_model_path:
        model.load(opt.load_model_path)

    test_data = Rich(opt.test_path,train=False,val=False,test=True)
    test_loader = DataLoader(dataset=test_data,batch_size=opt.batch_size,shuffle=False)
    for ii,(data,label) in enumerate(test_loader):
        if opt.use_gpu:
            data,label=data.cuda(),label.cuda()
        data,label = Variable(data,requires_grad=False),Variable(label.type(torch.LongTensor),requires_grad=False)
        predict = model(data)
        confusion_matrix.add(predict.data,label)
    cm_value = confusion_matrix.value()
    accuracy = (cm_value[0][0] + cm_value[1][1] + cm_value[2][2] + cm_value[3][3]) / cm_value.sum()
    print("The acc of the model is:"+ str(accuracy))

if __name__=='__main__':
    import fire
    fire.Fire()
